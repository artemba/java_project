# Дневник Фильмов с Отзывами 
Дневник просмотренных фильмов с отзывами (клиент-серверное приложение на Java).
## Автор
Бачуринский Артём Андреевич, группа P3220

## Подробнее о проекте
High-Level Overview - [HLO.md](HLO.md).

Design Document - [DD.md](DD.md).

Project overview. Demo - [DEMO_VIDEO.md](DEMO_VIDEO.md)