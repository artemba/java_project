package application;

import collection.CollectionManager;
import commands.ServerCommandManager;
import database.*;
import input.ConsoleInputManager;
import input.InputManager;
import output.ConsoleOutputManager;
import output.OutputManager;
import multithreading.Request;
import multithreading.RequestReceiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.*;

/**
 * Класс серверного приложения.
 */
public class ServerApplication implements Application {
    private InputManager inputManager;
    private ServerCommandManager serverCommandManager;
    private OutputManager outputManager;
    private boolean shutdown;
    private ServerSocketChannel serverSocketChannel;
    private ExecutorService receivingExecutor;
    private ExecutorService sendingExecutor;

    /**
     * Метод, который инициализирует нужные объекты и запускает главный цикл, реализуемый серверным приложением.
     * @param args аргументы командной строки, т.е. имя файла.
     */
    @Override
    public void start(String[] args) {
        shutdown = false;
        this.outputManager = new ConsoleOutputManager();
        this.inputManager = new ConsoleInputManager(new BufferedReader(new InputStreamReader(System.in)));

        if (args.length < 1) {
            outputManager.printlnErrorMessage("Порт для подключения к клиенту (args[0]) " +
                    "должен передаваться серверу как аргумент командной строки.");
            System.exit(0);
        }

        DBConnector dbConnector = new DBConnector(outputManager);
        dbConnector.connect();

        DBManager dbManager = new DBManager(dbConnector, outputManager);
        dbManager.createDBUsersTableIfNotExists();
        dbManager.createDBEntitiesTableIfNotExists();

        CollectionManager collectionManager = new CollectionManager(dbManager);

        PasswordProtector passwordProtector = new PasswordProtector();
        DBReader dbReader = new DBReader(dbConnector, collectionManager, outputManager, passwordProtector);
        dbReader.readDBEntitiesTable();
        DBWriter dbWriter = new DBWriter(dbConnector, passwordProtector);

        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(Integer.parseInt(args[0])));
            serverSocketChannel.configureBlocking(false);
        } catch (NumberFormatException e) {
            outputManager.printlnErrorMessage("Указанный порт для подключения к клиенту не преобразуется к типу int.");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.serverCommandManager = new ServerCommandManager(this, outputManager, collectionManager, dbWriter, dbReader, dbConnector);
        receivingExecutor = Executors.newFixedThreadPool(10);
        sendingExecutor = Executors.newCachedThreadPool();

        loop();
    }

    /**
     * Метод, реализующий главный цикл серверного приложения.
     */
    @Override
    public void loop() {
        outputManager.printlnMessage("Введите серверную команду (доступны: " + serverCommandManager.getCommandsOfServerNames() + "):");

        while (!shutdown) {
            try {
                SocketChannel communicationSocketChannel = serverSocketChannel.accept();
                if (communicationSocketChannel != null) {
                    receivingExecutor.submit(new RequestReceiver(communicationSocketChannel, serverCommandManager, sendingExecutor));
                }
            } catch (IOException ignored) { }

            try {
                if (inputManager.readyToRead()) {
                    Request request = new Request(inputManager.readLine().trim().toLowerCase());
                    serverCommandManager.manageRequestOfServer(request);
                }
            } catch (IOException ignored) { }
        }
        shutdownExecutors();
    }

    private void shutdownExecutors() {
        receivingExecutor.shutdownNow();
        sendingExecutor.shutdownNow();
    }

    public void setShutdown() {
        shutdown = true;
    }
}