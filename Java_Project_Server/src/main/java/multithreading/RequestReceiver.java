package multithreading;

import commands.ServerCommandManager;
import response.Response;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;

public class RequestReceiver implements Runnable {
    private SocketChannel communicationSocketChannel;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private ServerCommandManager serverCommandManager;
    private ExecutorService sendingExecutor;

    public RequestReceiver(SocketChannel communicationSocketChannel, ServerCommandManager serverCommandManager, ExecutorService sendingExecutor) {
        this.communicationSocketChannel = communicationSocketChannel;
        this.serverCommandManager = serverCommandManager;
        this.sendingExecutor = sendingExecutor;
    }

    @Override
    public void run() {
        try {
            this.objectInputStream = new ObjectInputStream(Channels.newInputStream(communicationSocketChannel));
            this.objectOutputStream = new ObjectOutputStream(Channels.newOutputStream(communicationSocketChannel));

            while (true) {
                Request request = (Request) objectInputStream.readObject();
                //new Thread(new RequestManager(request, serverCommandManager, sendingExecutor, objectOutputStream)).start();
                Response response = serverCommandManager.manageRequestOfClient(request);
                if (response != null)
                    objectOutputStream.writeObject(response);
            }
        } catch (EOFException e) {
            closeConnection();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
            closeConnection();
        }
    }

    private void closeConnection() {
        try {
            communicationSocketChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}