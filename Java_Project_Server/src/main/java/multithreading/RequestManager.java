package multithreading;

import commands.ServerCommandManager;

import java.io.ObjectOutputStream;
import java.util.concurrent.ExecutorService;

public class RequestManager implements Runnable {
    private Request request;
    private ServerCommandManager serverCommandManager;
    private ExecutorService sendingExecutor;
    private ObjectOutputStream objectOutputStream;

    public RequestManager(Request request, ServerCommandManager serverCommandManager, ExecutorService sendingExecutor, ObjectOutputStream objectOutputStream) {
        this.request = request;
        this.serverCommandManager = serverCommandManager;
        this.sendingExecutor = sendingExecutor;
        this.objectOutputStream = objectOutputStream;
    }

    @Override
    public void run() {
        //Response response = serverCommandManager.manageRequestOfClient(request);
        //sendingExecutor.submit(new ResponseSender(response, objectOutputStream));
    }
}
