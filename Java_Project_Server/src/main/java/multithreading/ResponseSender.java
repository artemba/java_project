package multithreading;

import response.Response;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class ResponseSender implements Runnable{
    private Response response;
    private ObjectOutputStream objectOutputStream;

    public ResponseSender(Response response, ObjectOutputStream objectOutputStream) {
        this.response = response;
        this.objectOutputStream = objectOutputStream;
    }

    @Override
    public void run() {
        try {
            if (response != null)
                objectOutputStream.writeObject(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
